public class Calculation {
    private int a,b,c = 0;


    public Calculation(){
    }

    public int abExists(String quadraticFunction, int x, int equalsPosition, int xSquaredPosition, int xPosition){ // ax² + bx + c
        a = Integer.parseInt(quadraticFunction.substring(equalsPosition+1,xSquaredPosition));
        b = Integer.parseInt(quadraticFunction.substring(xSquaredPosition+2,xPosition));
        if((quadraticFunction.indexOf(xPosition)+1 < quadraticFunction.length())){
            c = Integer.parseInt(quadraticFunction.substring(xPosition+1));
        }

        return (a*x*x+b*x+c);
    }

    private int aExists(String quadraticFunction, int x, int equalsPosition, int xSquaredPosition) { // ax² + c
        a = Integer.parseInt(quadraticFunction.substring(equalsPosition+1,xSquaredPosition));
        if((quadraticFunction.indexOf(xSquaredPosition)+2 < quadraticFunction.length())){
            c = Integer.parseInt(quadraticFunction.substring(xSquaredPosition+2));
        }
        return (a*x*x+b*x+c);
    }

    private int bExists(String quadraticFunction, int x, int equalsPosition, int xPosition) { // bx + c
        b = Integer.parseInt(quadraticFunction.substring(equalsPosition+1,xPosition));
        if((quadraticFunction.indexOf(xPosition)+1 < quadraticFunction.length())){
            c = Integer.parseInt(quadraticFunction.substring(xPosition+1));
        }
        return (a*x*x+b*x+c);
    }



    public int compute(String quadraticFunction, int x){
        int result = 0;
        int equalsPosition = quadraticFunction.indexOf('=');
        int xSquaredPosition = quadraticFunction.indexOf("x²");
        int xPosition = quadraticFunction.indexOf('x',xSquaredPosition+1);

        int numberOfX = (int) quadraticFunction.chars().filter(ch -> ch == 'x').count();
        if (numberOfX == 1){
            return Integer.parseInt(quadraticFunction.substring(equalsPosition+1));
        }

        if (xSquaredPosition != -1 && xPosition != -1){ // Pattern : ax² + bx + c
            result = abExists(quadraticFunction,x,equalsPosition,xSquaredPosition,xPosition);
        } else if (xSquaredPosition != -1) { // Pattern : ax² + c
            result = aExists(quadraticFunction,x,equalsPosition,xSquaredPosition);
        } else if (xPosition != -1) { // Pattern : bx + c
            xPosition = quadraticFunction.indexOf('x',equalsPosition+1);
            result = bExists(quadraticFunction,x,equalsPosition,xPosition);
        }
        return result;
    }


}
