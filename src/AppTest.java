public class AppTest {
    public static void main(String[] args) {

        String example1 = "f(x)=2x²+3x-1";
        String example2 = "f(x)=2x²-1";
        String example3 = "f(x)=2x-1";
        String example4 = "f(x)=-1";
        int x = 4;


        Calculation calculation1 = new Calculation();
        int result1 = calculation1.compute(example1,x);
        System.out.println(example1 + " avec x = "+ x +" donne " + result1);

        Calculation calculation2 = new Calculation();
        int result2 = calculation2.compute(example2,x);
        System.out.println(example1 + " avec x = "+ x +" donne " + result2);

        Calculation calculation3 = new Calculation();
        int result3 = calculation3.compute(example3,x);
        System.out.println(example3 + " avec x = "+ x +" donne " + result3);

        Calculation calculation4 = new Calculation();
        int result4 = calculation4.compute(example4,x);
        System.out.println(example4 + " avec x = "+ x +" donne " + result4);

    }
}
